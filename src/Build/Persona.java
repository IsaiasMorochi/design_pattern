package Build;

public class Persona {

    private String nombre;
    private int edad;
    private String municipio;
    private String colegio;
    private String lugarTrabajo;

    private Persona(){}

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", municipio='" + municipio + '\'' +
                ", colegio='" + colegio + '\'' +
                ", lugarTrabajo='" + lugarTrabajo + '\'' +
                '}';
    }

    public static class Builder {

        private Persona persona;

        public Builder(String nombre){
            persona = new Persona();
            persona.nombre = nombre;
        }

        public Builder setMunicipio (String municipio) {
            persona.municipio = municipio;
            return this; //method chaining
        }

        public BuilderMayor setMayor(int edad) {
            if (edad < 18) throw new IllegalArgumentException("es menor de edad " + edad);
            persona.edad = edad;
            return new Persona.BuilderMayor(persona);
//            persona.lugarTrabajo = lugarTrabajo;
//            persona.colegio = null;
//            return this;
        }

        public BuilderMenor setMenor(int edad) {
            if (edad >= 18) throw new IllegalArgumentException("es mayor de edad " + edad);
            persona.edad = edad;
            return new Persona.BuilderMenor(persona);
//            persona.colegio = colegio;
//            persona.lugarTrabajo = null;
//            return this;
        }

    }

    public static class BuilderMayor {
        //ref al objeto que se esta construyendo
        private Persona persona;

        private BuilderMayor(Persona persona) {
            this.persona = persona;
        }

        public BuilderMayor setLugarTrabajo (String lugarTrabajo) {
            this.persona.lugarTrabajo = lugarTrabajo;
            return this;
        }

        public Persona build(){
            return persona;
        }
    }

    public static class BuilderMenor {

        private Persona persona;

        private BuilderMenor(Persona persona){
            this.persona = persona;
        }

        public BuilderMenor setColegio (String colegio){
            this.persona.colegio = colegio;
            return this;
        }

        public Persona build(){
            return persona;
        }
    }

}
