/**
 * Prototype: Objetivo es crear una nueva instancia con los valores clonados
 * o copiados de otra instancia sin pasar por la asignacion manual de valores.
 */
package Prototype.one.inter;

/**
 *
 * @author isaias
 */
public interface ICuenta extends Cloneable{
    /**
     * Devuleve la misma interfaz, por que suponemos que puede haber alguna clase
     * que implemente esa interfaz y cumpla con este objetivo
     * @return 
     */
    ICuenta clonar();
}
