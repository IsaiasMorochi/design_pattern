/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory.one.inter.impl;

import AbstractFactory.one.inter.IConexionREST;

/**
 *
 * @author isaia
 */
public class ConexionRESTVentas implements IConexionREST{
    
	@Override
	public void leerURL(String url) {
		System.out.println("conectandose a: "+ url);
		
	}

}
