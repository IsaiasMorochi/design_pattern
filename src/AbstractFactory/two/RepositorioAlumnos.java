package AbstractFactory.two;

import java.util.List;

public interface RepositorioAlumnos {

    public List<String> listaAlumnos();
}
