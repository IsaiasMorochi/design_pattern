package Template.one.model;

/**
 *
 * @author isaias
 */
public abstract class ClaseAbstracta {
    
   public int getNum(int num){
       this.mostrar();
       int n = this.sumar(num);
       n = this.multiplicar(n);
       return n;
   }
   
   public String getSerie(int num){
       this.mostrar();
       String serie = "";
       for (int i = 1; i < num; i++) {
           if(this.termino(i)){
                serie += i +  ", ";
           }
       }
       return serie;
   }
   
   public void mostrar(){
       System.out.println("Inciando algoritmo");
   }
   public abstract int sumar(int n);
   public abstract int multiplicar(int n);
   public abstract boolean termino(int n);
 
}
