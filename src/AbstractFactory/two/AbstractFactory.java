package AbstractFactory.two;

/**
 * Crear una interface para crear familias de objetos sin
 * necesidad de indicar su tipo concreto
 *
 * Tiene metodos para crear los distintos tipos de repositorios
 * usando las interfaces como tipo de retorno, no las implementaciones concreatas
 *
 */
public interface AbstractFactory  {

    RepositorioAlumnos createRepositorioAlumnos();

    RepositorioCursos createRepositorioCursos();

}
