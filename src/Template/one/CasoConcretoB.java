/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Template.one;

import Template.one.model.ClaseAbstracta;

/**
 *
 * @author isaias
 */
public class CasoConcretoB extends ClaseAbstracta {

    @Override
    public int sumar(int n) {
        return n + 5;
    }

    @Override
    public int multiplicar(int n) {
        return n * 1000;
    }

    @Override
    public boolean termino(int n) {
        // 1 3 5 7 9
        //return n % 10 == 0;
     
        return n % 2 != 0;
    }
    
}
