/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory.one.model;

import AbstractFactory.one.inter.FabricaAbstracta;
import AbstractFactory.one.inter.IConexionBD;
import AbstractFactory.one.inter.IConexionREST;
import AbstractFactory.one.inter.impl.ConexionRESTCompras;
import AbstractFactory.one.inter.impl.ConexionRESTNoArea;
import AbstractFactory.one.inter.impl.ConexionRESTVentas;

/**
 *
 * @author isaias
 */
public class ConexionRESTFabrica implements FabricaAbstracta{
    
    @Override
        public IConexionREST getREST(String area) {
            if (area == null) {
                return new ConexionRESTNoArea();
            }
            if (area.equalsIgnoreCase("COMPRAS")) {
                return new ConexionRESTCompras();
            } else if (area.equalsIgnoreCase("VENTAS")) {
                return new ConexionRESTVentas();
            }
            return new ConexionRESTNoArea();
        }

        @Override
        public IConexionBD getBD(String motor) {
                return null;
	}
}
