package AbstractFactory.one.inter.impl;

import AbstractFactory.one.inter.IConexionREST;

/**
 *
 * @author isaia
 */
public class ConexionRESTCompras implements IConexionREST{
    	@Override
	public void leerURL(String url) {		
		System.out.println("Conectándose a " + url);
	}

}
