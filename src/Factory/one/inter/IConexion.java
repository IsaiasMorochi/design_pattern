
package Factory.one.inter;

/**
 *
 * @author isaia
 */
public interface IConexion {  
    void conectar();
    void desconectar();
}
