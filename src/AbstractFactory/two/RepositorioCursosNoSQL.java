package AbstractFactory.two;

import java.util.Arrays;
import java.util.List;

public class RepositorioCursosNoSQL implements  RepositorioCursos {

    @Override
    public List<String> listaCursos() {
        return Arrays.asList("Cursos no relacional");
    }
}
