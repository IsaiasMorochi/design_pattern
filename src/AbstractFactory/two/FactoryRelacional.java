package AbstractFactory.two;

public class FactoryRelacional implements AbstractFactory {

    @Override
    public RepositorioAlumnos createRepositorioAlumnos() {
        return new RepositorioAlumnosRelacional();
    }

    @Override
    public RepositorioCursos createRepositorioCursos() {
        return new RepositorioCursosRelacional();
    }
}
