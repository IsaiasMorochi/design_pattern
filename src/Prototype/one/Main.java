/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Prototype.one;

import Prototype.one.model.CuentaAhorro;

/**
 *
 * @author isaias
 */
public class Main {
    
    public static void main(String[] args) {
        
        CuentaAhorro cuentaAhorro = new CuentaAhorro();
        cuentaAhorro.setMonto(200);

        //CuentaAhorro cuentaAhorro2 = new CuentaAhorro();

        CuentaAhorro cuentaClonada = (CuentaAhorro) cuentaAhorro.clonar();

        System.out.println(cuentaAhorro);
        //System.out.println(cuentaAhorro2);
        System.out.println(cuentaClonada);

        /*if (cuentaClonada != null) {
                System.out.println(cuentaClonada);
        }*/
        
        // Cuenta clonada es distinta a la cuenta de ahorro en referencia de memoria
        
        System.out.println(cuentaClonada == cuentaAhorro);
        // comparando referencia en memoria mas no valores.
    }
}
