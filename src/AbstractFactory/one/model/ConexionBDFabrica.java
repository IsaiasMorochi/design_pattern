/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory.one.model;

import AbstractFactory.one.inter.FabricaAbstracta;
import AbstractFactory.one.inter.IConexionBD;
import AbstractFactory.one.inter.IConexionREST;
import AbstractFactory.one.inter.impl.ConexionMySQL;
import AbstractFactory.one.inter.impl.ConexionOracle;
import AbstractFactory.one.inter.impl.ConexionPostgreSQL;
import AbstractFactory.one.inter.impl.ConexionSQLServer;
import AbstractFactory.one.inter.impl.ConexionVacia;


/**
 *
 * @author isaias
 */
public class ConexionBDFabrica implements FabricaAbstracta {
    
    @Override
    public IConexionBD getBD(String motor) {
            if (motor == null) {
                return new ConexionVacia();
            }
            if (motor.equalsIgnoreCase("MYSQL")) {
                return new ConexionMySQL();
            } else if (motor.equalsIgnoreCase("ORACLE")) {
                return new ConexionOracle();
            } else if (motor.equalsIgnoreCase("POSTGRE")) {
                return new ConexionPostgreSQL();
            } else if (motor.equalsIgnoreCase("SQL")) {
                return new ConexionSQLServer();
            }
            return new ConexionVacia();
    }

    @Override
    public IConexionREST getREST(String area) {
            return null;
    }
}
