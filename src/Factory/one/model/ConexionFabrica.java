package Factory.one.model;

import Factory.one.inter.IConexion;
import Factory.one.inter.imple.ConexionMySQL;
import Factory.one.inter.imple.ConexionOracle;
import Factory.one.inter.imple.ConexionPostgreSQL;
import Factory.one.inter.imple.ConexionSQLServer;
import Factory.one.inter.imple.ConexionVacia;


/**
 * Factory :
 * Devolver Instancias de una clase en particular por medio de algun identificador
 * necesidad => Fabrica => objeto creado
 *                      => objeto creado
 *                      => objeto creado
 * MySQL => Fabrica => MySQL creada 
 *                  => PostgreSQL creada 
 *                  => ConexionN creada 
 * Fabrica tiene sus implementaciones, y devuelve esta segun el identificador enviado.
 * Objetivo es devolver una instancia segun lo pida el usuario.
 */

/**
 * @author isaia
 */
public class ConexionFabrica {
    
    public IConexion getConexion(String motor) {
        if (motor == null) {
            return new ConexionVacia();
        }
        if (motor.equalsIgnoreCase("MYSQL")) {
            return new ConexionMySQL();
        } else if (motor.equalsIgnoreCase("ORACLE")) {
            return new ConexionOracle();
        } else if (motor.equalsIgnoreCase("POSTGRE")) {
            return new ConexionPostgreSQL();
        } else if (motor.equalsIgnoreCase("SQL")) {
            return new ConexionSQLServer();
        }
        return new ConexionVacia();
    }
}

