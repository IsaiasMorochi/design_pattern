package Prototype.one.model;

import Prototype.one.inter.ICuenta;

/**
 *
 * @author isaias
 */
public class CuentaAhorro implements ICuenta{
    
    private String tipo;
    private double monto;
    
    public CuentaAhorro() {
	tipo = "AHORRO";
    }

    @Override
    public ICuenta clonar() {
        CuentaAhorro cuenta = null;
        try {
            // clonamos el objeto
            cuenta = (CuentaAhorro) clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return cuenta; //retornamos la instancia clonada
    }

    @Override
    public String toString() {
        return "CuentaAhorro [tipo=" + tipo + ", monto=" + monto + "]";
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public double getMonto() {
        return monto;
    }  
}
