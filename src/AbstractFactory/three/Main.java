package AbstractFactory.three;

public class Main {



    public static void main(String[] args) {

//        Enemy Ship Building se encarga de los pedidos de nuevos Enemy Ships.
//        Usted envía un código utilizando el método orderTheShip y envía el pedido
//        a la fábrica correcta para su creación.

         // Construcción de naves enemigas
        EnemyShipBuilding MakeUFOs = new UFOEnemyShipBuilding();

        EnemyShip theGrunt = MakeUFOs.orderTheShip("UFO");
        System.out.println(theGrunt + "\n");

        EnemyShip theBoss = MakeUFOs.orderTheShip("UFO BOSS");
        System.out.println(theBoss + "\n");
    }

}
