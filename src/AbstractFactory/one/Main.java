/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory.one;

import AbstractFactory.one.inter.FabricaAbstracta;
import AbstractFactory.one.inter.IConexionBD;
import AbstractFactory.one.inter.IConexionREST;
import AbstractFactory.one.model.FabricaProductor;

/**
 *
 * @author isaia
 */
public class Main {
    
    public static void main(String[] args) {

    	FabricaAbstracta fabricaBD = FabricaProductor.getFactory("BD");

        IConexionBD cxBD1 = fabricaBD.getBD("MYSQL");
        cxBD1.conectar();

        FabricaAbstracta fabricaREST = FabricaProductor.getFactory("REST");
        IConexionREST cxRS1 = fabricaREST.getREST("COMPRAS");

        cxRS1.leerURL("https://www.youtube.com/subscription_center?add_user=mitocode");
    }

    
}
