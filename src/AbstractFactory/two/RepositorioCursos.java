package AbstractFactory.two;

import java.util.List;

public interface RepositorioCursos {

    List<String> listaCursos();
}
