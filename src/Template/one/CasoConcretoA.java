/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Template.one;

import Template.one.model.ClaseAbstracta;

/**
 *
 * @author isaias
 */
public class CasoConcretoA extends ClaseAbstracta{
    
    public CasoConcretoA(){
        System.out.println("Constructor");
    }
    
    @Override
    public int sumar(int n) {
      return n  + 1;
    }

    @Override
    public int multiplicar(int n) {
        return n * 0;
    }

    @Override
    public boolean termino(int n) {
        return n %  2 != 0;
    }
    
}
