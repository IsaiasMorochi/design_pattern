/**
 * El patron de fábrica abstracta  funciona en torno a una superfábrica que crea otras fábricas. 
 * Esta fábrica también se llama fábrica de fábricas. Este tipo de patrón de diseño se incluye en el 
 * patrón de creación, ya que este patrón proporciona una de las mejores formas de crear un objeto.
 */
package AbstractFactory.one.inter;

/**
 * @author isaia
 */
public interface FabricaAbstracta {
    
    IConexionBD getBD(String motor);

    IConexionREST getREST(String area);
    
}
