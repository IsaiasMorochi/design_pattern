package Singleton.two;

public class Persona {

    private static Persona ourInstance = new Persona();

    public static Persona getInstance() {
        return ourInstance;
    }

    private Persona() {
    }

}
