package Adapter.two;

import java.util.Date;

/**
 * Se implenta esta nueva clase con el rol de wrapper
 * el adapter hace de envoltio y lo presenta como un objeto del tipo
 * StandarReservasACME
 */
public class StandarReservasACMEAdapter implements StandardReservasACME {

    // la clase actua como envoltorio del sistema existente (ReservaSL)
    private ReservaSL miSistema;

    public StandarReservasACMEAdapter(ReservaSL miSistema){
        this.miSistema = miSistema;
    }



    @Override
    public String getInfoHotel(String idHotel) {
        return miSistema.hotelInfo(idHotel);
    }

    @Override
    public String createReservation(Date fecha, int dias, String idHotel, String cliente) {
        long codigo = miSistema.creaReserva(idHotel, cliente, fecha, dias);
        return String.valueOf(codigo);
    }

    @Override
    public String getReservation(String codigo) {
        long codigoLong = Long.parseLong(codigo);
        return miSistema.datosReserva(codigoLong);
    }
}
