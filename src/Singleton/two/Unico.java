package Singleton.two;

import java.io.Serializable;

public class Unico  implements Serializable {

    private static Unico INSTANCE;

    private Unico(){ }

    public synchronized static Unico getUnico(){
        if (INSTANCE == null){
            INSTANCE = new Unico();
        }
        return INSTANCE;
    }

    private Object readResolve(){
        return INSTANCE;
    }

//    public enum Unico{ INSTANCE;}

}
