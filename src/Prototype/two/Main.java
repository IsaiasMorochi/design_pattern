package Prototype.two;

/**
 * GOF
 * Specify the kinds of objects to create using a
 * prototypical instance, and create new objects by copying
 * this prototype.
 *
 * Especifica el tipo de objeto a crear usando una instancia como
 * prototipo y crear nuevos objetos copiando este prototipo.
 */
public class Main {
}
