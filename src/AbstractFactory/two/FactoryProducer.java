package AbstractFactory.two;

public class FactoryProducer {

    public static  AbstractFactory getFactory(String type){

        if (type.equalsIgnoreCase("Relacional")){
            return new FactoryRelacional();
        }else if (type.equalsIgnoreCase("NoRelacional")){
            return new FactoryNoRelacional();
        }

        return  null;
    }
}
