package Singleton.one.model;

/**
 * Singleton:  Tener una unica instancia de una clase durante toda la 
 * aplicacion, que todos los usuarios utilicen esa unica instancia.
 * Caso Estudio
 * De una lista de paises es la posibilidad casi nula que cambien sus nombres,
 * no tiene sentido devolver a cada usuario una unica instancia, es decir un 
 * espacio en memoria, por que de ...NUsuarios tendriamos que devolver 
 * ...NObjetos o espacios  de memoria, utilizando la misma informacion.
 * Lista de paises1 => Usuario1
 * Lista de paises2 => Usuario2
 * ...
 * Lista de paisesN => UsuarioN
 * 
 * SOLUCION: Tener un unico objeto que sea instanciado una unica vez, y por cada usuario
 * que necesite esta informacion, simplemente obtenda la informacion que ya sido 
 * instanciado por otro usuario, y esto en performance es mucho mejor.
 * Lista de paises1 => Usuario1
 * Lista de paises1 => Usuario2
 * ...
 * Lista de paises1 => UsuarioN
 */

/**
 *
 * @author isaia
 */
public class Conexion {
    
    // Declaracion
    private static Conexion instancia;
    // private static Conexion instancia = new Conexion();
    
    // Para evitar instancia mediante operador "new"
    private Conexion(){
    
    }
    
    /**
     * Para obetener la instancia unicamente por este metodo
     * Notese la palabra reservada "static" hace posible el acceso mediante Clase.metodo
     * @return 
     */
    public static Conexion getInstancia(){
        if (instancia == null) {
            instancia = new Conexion();
        }
        return instancia;
    }
    
    /**
     * Metodo de prueba
     */
    public void conectar(){
        System.out.println("Me conecte a la BD");
    }
    
    /**
     * Metodo de prueba
     */
    public void desconectar(){
        System.out.println("Me desconecte de la BD");
    }       
}
