/**
 * PATRON ESTRUCTURAL
 * FACADE:
 * El patrón de fachada oculta las complejidades del sistema y proporciona una interfaz 
 * para el cliente mediante la cual el cliente puede acceder al sistema. 
 * Este tipo de patrón de diseño se enmarca en un patrón estructural, 
 * ya que agrega una interfaz al sistema existente para ocultar sus complejidades.
 * Este patrón implica una clase única que proporciona métodos simplificados requeridos por el cliente y delega las llamadas a los métodos de las clases del sistema existentes.
 */
package Facade.one.model;

import Facade.one.api.AvionAPI;
import Facade.one.api.HotelAPI;

/**
 *
 * @author isaias
 */
public class CheckFacade {
    
    private AvionAPI avionAPI;
    private HotelAPI hotelAPI;	

    public CheckFacade() {
            avionAPI = new AvionAPI();
            hotelAPI = new HotelAPI();
    }

    public void buscar(String fechaIda, String fechaVuelta, String origen, String destino) {
            avionAPI.buscarVuelos(fechaIda, fechaVuelta, origen, destino);
            hotelAPI.buscarHoteles(fechaIda, fechaVuelta, origen, destino);
    }	
}
