package Build;

/**
 * GOF:
 * Separe the contruction of a complex
 * object from its representation so that
 * the same construction process can create different representations
 *
 * Para la construccion de objetos complejos separemos el proceso de
 * construcion de su representacion(Persona), crear una unica clase cuya
 * responsabilidad sera crear objetos del tipo persona.
 *
 * Telescoping constructor - Antipattern
 * El problema con este (anti) patrón es que una vez que su clase tiene muchos constructores
 * y constructores con 4 o más parámetros, se vuelve difícil recordar el orden requerido de
 * los parámetros, así como el constructor en particular que desee en una situación determinada.
 * Darse a ti mismo y a tus compañeros desarrolladores un tiempo difícil al intentar inicializar
 * esa clase.
 *
 * Permite resolver la limitacion de crear objetos solo utilizando constructores
 * */

public class Main {


    public static void main (String[] args) {

//        new Persona.Builder("Jose")
//                .setMunicipio("municipio")
//                .setMayor(20, "trabajo")
//                .build();

        // este codigo debe compilar

		Persona madre = new Persona.Builder("Maria")
                .setMunicipio("Selva")
                .setMayor(37)
                .setLugarTrabajo("Google")
                .build();

        System.out.println(madre.toString());

		Persona hijo = new Persona.Builder("Pedro")
                .setMenor(6)
                .setColegio("Bolivia")
                .build();

        System.out.println(hijo.toString());

        Persona n = new Persona.Builder("n")
                .setMenor(4)
                .build();

        System.out.println(n.toString());



        // esta codigo NO debe compilar
		/*
		Persona mal = new Persona.Builder("Luisa")
				    .setMayor(20)
				    .setColegio("Colegio de Villa Arriba")
				    .build();
        */
    }

}
