package Adapter.two;

import java.util.Date;

public interface StandardReservasACME {
    
    String getInfoHotel(String idHotel);

    String createReservation (Date fecha, int dias, String idHotel, String cliente);

    String getReservation (String codigo);

}
