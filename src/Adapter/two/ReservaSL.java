package Adapter.two;

import java.util.Date;

public interface ReservaSL {

    String hotelInfo (String idHotel);

    long creaReserva (String idHotel, String cliente, Date fecha, int dias);

    String datosReserva (long codigo);
}
