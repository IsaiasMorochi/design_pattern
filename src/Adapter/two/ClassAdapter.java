package Adapter.two;

import java.util.Date;

/**
 *  Object adapter vs Class adapter
 *  En lugar de tener un objeto que permite traducir las llamadas
 *  de la nueva interface a la antigua, tener un tipo que implemente
 *  las dos nuevas interfaces simultaneamente.
 */

public class ClassAdapter extends MiSistemaReservas implements StandardReservasACME {



    @Override
    public String getInfoHotel(String idHotel) {    // comparacion con object adapter
        return hotelInfo(idHotel);                  // return miSistema.hotelInfo(idHotel);
    }

    @Override
    public String createReservation(Date fecha, int dias, String idHotel, String cliente) {
        long codigo = creaReserva(idHotel, cliente, fecha, dias);
        return String.valueOf(codigo);
    }

    @Override
    public String getReservation(String codigo) {
        long codigoLong = Long.parseLong(codigo);
        return datosReserva(codigoLong);
    }
}
