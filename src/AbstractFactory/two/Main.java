package AbstractFactory.two;

/**
 * AbstractFactory:
 * Proporcionar una interface para crear familias de objetos relacionados o
 * independientes sin especificar su clase concreta
 *
 * Este pattern permite que en caso que en caso nuestra app
 * utilice familias de objetos diferentes segun el entorno, podamos
 * centralizar la desicion de que tipo de objeto crear.
 *
 * con ello se simplifica el codigo cliente y el sistema puede
 * extenderse mas facil.
 */
public class Main {

    public void main(String[] args) {

        //RepositorioAlumnos repositorioAlumnos = new RepositorioAlumnosRelacional();

        //RepositorioCursos repositorioCursos = new RepositorioCursosRelacional();

        // pero se anaden dos nuevas implementaciones...


    }


}
