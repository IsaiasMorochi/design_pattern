package AbstractFactory.two;

public class FactoryNoRelacional implements AbstractFactory {

    @Override
    public RepositorioAlumnos createRepositorioAlumnos() {
        return new RepositorioAlumnosNoSQL();
    }

    @Override
    public RepositorioCursos createRepositorioCursos() {
        return new RepositorioCursosNoSQL();
    }
}
