package Singleton.two;

/**
 * GoF
 * Ensure a class only has one instance,
 * and privede a global point of acces to it.
 */

public class Main {
    public static void main(String[] args) {
        //Instanciación por constructor prohíbido por ser "private"
        Unico u = Unico.getUnico();

        boolean rpta = u instanceof Unico;
        System.out.println(rpta);
    }

}
