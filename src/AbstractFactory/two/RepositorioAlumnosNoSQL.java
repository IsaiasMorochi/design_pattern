package AbstractFactory.two;

import java.util.Arrays;
import java.util.List;

public class RepositorioAlumnosNoSQL implements RepositorioAlumnos {

    @Override
    public List<String> listaAlumnos() {
        return Arrays.asList("Alumnos no relacional");
    }
}
