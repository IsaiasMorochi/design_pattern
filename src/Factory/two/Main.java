package Factory.two;

/**
 * GOF
 * Define an interface for creating an object, but
 * let subclasses decide which clas to instantiate.
 * The Factory method lets a class defer
 * instantiation it uses to subclasses.
 *
 * Definir una interfaz para crear un objeto, pero
 * dejar que las subclases decidan qué clase concreta instanciar.
 * El método Factory permite a una clase diferir.
 * La instanciación se utiliza para las subclases.
 *
 * (interface no es la interface de java sino que la clase que ataca al cliente)
 *
 * Problema:
 * como encapsular la creacion e inicializacion del objeto,
 * si este puede tener distintos tipos
 */
public class Main {

}
